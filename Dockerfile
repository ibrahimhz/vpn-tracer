FROM python:3-alpine
LABEL environment="development"
LABEL usecase="prototype"
LABEL Maintainer="Mohammed Ibrahim"
WORKDIR /app
COPY ./requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY ./ ./
EXPOSE 80
ENTRYPOINT [ "/usr/local/bin/gunicorn" ]
CMD [ "--env", "DJANGO_SETTINGS_MODULE=scraper_project.settings", "--bind", "0.0.0.0:80", "scraper_project.wsgi" ]

