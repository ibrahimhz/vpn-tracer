from django.shortcuts import render, redirect
from django.contrib import messages

# Create your views here.
def index(request):
    if request.method == "POST":
        ipv4 = request.POST['subject']
        if ipv4 == "13.233.214.241":
            import os
            rfile = open('scraper/logs/server-status.log', 'r')
            content = rfile.read()
            rfile.close()
            contentlist = content.split(' ')
            constants = ["Connected", "client", "keys:", "Source", "IP's:", "Client", "Allocated", "IP's:", "Connection" ,"Time:", ' ', '', '\n']
            basic = []
            medium = []
            for data in contentlist:
                if (data not in constants):
                    basic.append(data)
            for data in basic:
                a = data.split('\n')
                for x in a:
                    if x not in constants:
                        medium.append(x)
            length = len(medium)//8
            print("Number of Users: ",length)
            rev = (len(medium)//8)*5
            fromval = len(medium)-rev
            timelist = medium[fromval:]
            medium = medium[:fromval]
            j = 5
            i = 0
            formated = []
            for item in range(i, len(timelist), 5):
                exam = timelist[item: j]
                formated.append(exam)
                j+=5
            converted_time = []    # converted the formated list to strings and storing in converted
            def converted_tf(mylist):
                str = ""  
                for x in mylist:
                    for y in x:
                        str+=y+" "
                    converted_time.append(str)
                    str=""
            converted_tf(formated)
            pre_converted_name = []
            i = 0;
            a = 0;
            b = length
            while (i<length):
                for x in range(a,len(medium)):
                    pre_converted_name.append(medium[a:b])
                    b+=b
                    a+=length
                i+=1
            converted_name = []
            for i in range(0,len(pre_converted_name)):
                converted_name.append(pre_converted_name[i])
            converted_name[-1]=converted_time
            final_give = []
            for i in converted_name:
                if len(i) != 0:
                    final_give.append(i)
            x = 0;
            string = ""
            while(0, len(final_give)):
                if x == length:
                    break;
                for i in range(0, len(final_give)):
                    s = final_give[i][x]
                    string = string + s + ","
                else:
                    string = string+"\n"
                x = x+1
            wfile = open('scraper/logs/created.log', 'w')
            wfile.write(string)
            wfile.close()
            if os.path.exists('scraper/logs/final.log'):
                os.remove('scraper/logs/final.log')
            rfile = open('scraper/logs/created.log', 'r')
            wfile = open('scraper/logs/final.log', 'a')
            for x in rfile.readlines():
                cont = x.split(',')
                del cont[-1]
                wfile.write(str(cont)+"\n")
            wfile.close()
            rfile.close()
            # messages.success(request, "Correct IP")
            f = open('./scraper/logs/created.log', 'r')
            file_contents = f.readlines()
            f.close()
            #     'result': file_contents, 
            #     'ip': ipv4
            # }
            sample= []
            for i in file_contents:
                kali = []
                kali.append(i)
                sample.append(kali)

            for x in sample:
                return render(request, 'index1.html', {'result': sample})

        else:
            messages.error(request, "Wrong IP")
            return redirect("/")
    else:
        ipv4 = ''
    return render(request, 'index1.html')
